package main

import (
	"fmt"

	"github.com/yunify/qingstor-sdk-go/config"
	qs "github.com/yunify/qingstor-sdk-go/service"
	"os"
)

func main() {
	conf, _ := config.New("SMGUBLZMPPFQSOVHLMMZ", "8g17WYOOGhS6NApiXmxaBH1k21AsOMBiBwvST3vM")

	// Initialize service object for QingStor.
	qsService, _ := qs.Init(conf)

	// 问题：
	// 		1. <bucket-name> is existed but no message to warning
	// 		2. memory is not allocate but no message to warning
	// Create a buckext
	bucket, err := qsService.Bucket("test-bucket2", "pek3a")
	if err != nil {
		panic("创建失败")
	}
	putBucketOutput, _ := bucket.Put()
	fmt.Println(putBucketOutput)

	bOutput, _ := bucket.ListObjects(nil)
	fmt.Println(qs.IntValue(bOutput.StatusCode))
	fmt.Println(len(bOutput.Keys))

	file, _ := os.Open("/home/joe/Downloads/137-140531102919.jpg")
	defer file.Close()

	oOutput, _ := bucket.PutObject("test.jpg", &qs.PutObjectInput{Body: file})
	fmt.Println(qs.IntValue(oOutput.StatusCode))
	// List all buckets.
	qsOutput, _ := qsService.ListBuckets(nil)

	// Print HTTP status code.
	fmt.Println(qs.IntValue(qsOutput.StatusCode))

	// Print the count of buckets.
	fmt.Println(qs.IntValue(qsOutput.Count))

	// Print the first bucket name.
	fmt.Println(qs.StringValue(qsOutput.Buckets[0].Name))
}
